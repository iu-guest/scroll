================
 String library
================

Asymptote language provides enough functionality to make anything with
strings, but tools provided sometimes are too low-level, requiring
manual calculation of character indexes and offsets.

This library is modelled after Python `str` datatype, but since it is
impossible to extend basic Asymptote classes, overloaded functions are
used instead of methods. To import this library, use following code

.. code:: c++

   access libasy2;
   unravel libasy2.string;

.. warning::

   All charater classification is ascii-only. Implementing full
   unicode support purely in Asymptote seems unreasonable complex
   task. Contributions by brave souls are welcome.


String functions
================

.. cpp:function:: string capitalize(string s)

   Return a capitalized version of string, i.e. make the first
   character have upper case and the rest lower case.

   .. code:: c++

      capitalize("fooBar") == "Foobar"

.. cpp:function:: string center(string s, int width, string fillchar = ' ')

   Return centered in a string of length *width*. Padding is done
   using the specified *fillchar*. The original string is returned if
   *width* is less than or equal to ``len(s)``. One more padding
   character is inserted after than before when number of padding
   symbols is odd.

   .. code:: c++

      center("foo", 2) == "foo"
      center("foo", 5) == " foo "
      center("foo", 6, "!") == "!foo!!"

.. cpp:function:: int count \
	(string s, string sub, int start = 0, int end = length(s))

   Return the number of non-overlapping occurrences of substring
   *sub* in the range [*start*, *end*). Optional arguments start
   and end are interpreted as in slice notation.

   .. code:: c++

      count("foo", "o") == 2
      count("xyxyx", "xyx") == 1
      count("111", "1", end = -1) == 2

.. cpp:function:: bool endswith(string s, string suffixes[], \
		  int start = 0, int end = length(s))
		  bool endswith(string s, int start = 0, int end = length(s) \
		  string suffix, ...)

   Return true if string ends with the any of specified suffixes,
   false otherwise.  With optional *start*, test beginning at that
   position.  With optional *end*, stop comparing at that position.

   This function has version that accepts suffixes as array and as
   variable-length argument list. First is more reusable, second is
   much more convenient in client code.

   .. code:: c++

      endswith("foo.txt", "txt") == true
      endswith("foo.txt", end = -1, "txt") == false
      endswith("foo.txt", end = -1, "tx") == true
      endswith("foo.txt", ".rst", ".txt") == true

.. cpp:function:: void evalf(string template, subst st, ...)

   Perform string substitution with :cpp:func:`sprintf` and evaluate
   resulting string in current content (embedded = true).

   .. note::

      No error checking is possible.

.. cpp:function:: bool isalnum(string s)

   Return true if all characters in the string are alphanumeric and
   there is at least one character, false otherwise. A character is
   alphanumeric if either :cpp:func:`isalpha` or :cpp:func:`isdigit`
   returns true.

.. cpp:function:: bool isalpha(string s)

   Return true if all characters in the string are alphabetic and
   there is at least one character in it, false otherwise.

   .. code:: c++

      isalpha("foo") == true
      isalpha("foo.txt") == false
      isalpha("") == false

.. cpp:function:: bool isdigit(string s)

   Return true if all characters in the string are digits and there is
   at least one character, false otherwise.

   .. code:: c++

      isdigit("10") == true
      isdigit("0x14") == false
      isdigit("") == false

.. cpp:function:: bool islower(string s)

   Return true if all cased characters in the string are lowercase
   and there is at least one cased character in the string, false
   otherwise.

   .. code:: c++

      islower("foobar.") == true
      islower("Foobar") == false
      islower("") == false

.. cpp:function:: bool isspace(string s)

   Return true if all characters in the string are whitespace and
   there is at least one character in the string, false otherwise.

.. cpp:function:: bool isupper(string s)

   Return true if all cased characters in the string are uppercase
   and there is at least one cased character in the string, false
   otherwise.

   .. code:: c++

      isupper("ABC") == true
      isupper(".&=") == true
      isupper("Foo") == false
      isupper("") == false

.. cpp:function:: string join(string sep, string elements[])
                  string join(string sep, string element, ...)

    Return a string which is the concatenation of the elements with
    separator *sep*. This function has array and rest-arguments
    versions.

    .. code:: c++

       join(",", "alpha", "beta") == "alpha,beta"
       join(".", new string[] { "1", "2" }) == "1.2"

.. cpp:function:: string lower(string s)

   Return a copy of the string converted to lowercase. Note, that
   non-ASCII symbols are not converted.

   .. code:: c++

	     lower("Foo1") == "foo1"

.. cpp:function:: subst operator %(string what, string with)
		  subst operator cast(string with)
		  subst operator cast(int n)
		  string sprintf(string template, subst substs[])
		  string sprintf(string template, subst st, ...)

   Replace ``{variable}`` substrings in template with specified strings.

   Substitution rules are represented by opaque structure *subst*,
   which can be created with either *operator %* or with implicit
   cast.  In first case named substitution is created, in second --
   indexed. They can be freely mixed. Example worth a thousand words:

   .. code:: c++

      sprintf("I am {name}", "name" % "Wizard") == "I am Wizard"
      sprintf("{0} is {name}", "I", "name" % "Foo") == "I is Foo"
      sprintf("{0} is {1}, {1}!", "foo", "bar") == "foo is bar, bar!"
      sprintf("Foo {0}", 12) == "Foo 12"

   .. note::

      This function is modelled after ``str.format`` method in Python,
      but escaping mechanism (double curly brackets) is not supported.

.. cpp:function:: string slice(string s, int begin = 0, int end = length(s))

   Return substring starting at position *begin* till position
   *end*. Like Python slicing syntax, negative indexes are supported,
   referencing to positions, enumerated from the end of string.

   .. code:: c++

	     slice("foo.txt", end = -4) == "foo"
	     slice("01234", 2) == "234"
	     slice("foo.txt", begin = 1, end = -1) == "oo.tx"

.. cpp:function:: string upper(string s)

   Return a copy of the string converted to uppercase.
