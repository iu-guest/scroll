=============
Boxed library
=============

Like Java programming language, Asymptote distinguish between
primitive and reference datatypes. Primitive datatypes are not
nullable and can not be created with operator *new*.  So following
code is not valid

.. code:: c++

   int n = new int; /* type 'int' is not structure */
   int k = null;    /* cannot cast 'null' to 'int' */

while the following is okay:

.. code:: c++

   struct point { int x; int y; }
   point p1 = new point; // p1.x == p1.y == 0
   point p2 = null;
   int N; // initialized with 0.

It is unfortunate, since such distinction complicates writing
:doc:`generic <generics>` functions. This module provides wrapper
structures (boxes) around primitive types (like in Java), allowing to
express, for example, optional integer value. Most likely you will
want import this module unqualified with lines below.

.. code:: c++

   access libasy2
   unravel libasy2.boxed

.. cpp:class:: Guide
	       Int
	       Pair
	       Path
	       Pen
	       Real
	       String
	       Triple

All these records has single field named *value* of corresponding
primitive type and are implicitly casted from value of that primitive
type. As such, code below is valid:

.. code:: c++

   Int n1 = 10;      // implicit cast
   Int n2 = new Int; // initialized with 0
   Int n3 = null;    // no value is fine

   int k1 = n1.value; // extraction must be explicit
   int k2 = n3.value; // dreaded `dereference of null pointer' error
