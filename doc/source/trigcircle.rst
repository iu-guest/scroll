======================
 Trigonometric circle
======================

This library provides routines for drawing point and ranges on
trigonometric circle. It is especially useful when creating
illustrations for trigonometric equations and inequalities.

Application interface
=====================

.. highlight:: c++
.. cpp:class:: trigcircle

   The whole trigonometric circle and everything marked on it is
   represented by instance of this structure. It should be created
   with ``new`` operator::

     access libasy2;
     unravel libasy2.trigcircle;

     var t = new trigcircle;

.. cpp:function:: void trigcircle::add_point(ratio r, bool inc = true)

   Mark point *r* ·π on circle. If optional argument *inc* is true (default),
   small circle, denoting point is filled black, otherwise it is filled white
   with black contour.

.. cpp:function:: void trigcircle::add_points(ratio r, ...)

   Mark multiple included points with single call. This method is exclusively
   for saving typing, multiple calls to :cpp:func:`trigcircle::add_point`
   would be equally good.

.. cpp:function:: void trigcircle::add_range(ratio left, ratio right)

   Mark arc between *lefţ̣* ·π and *right* ·π on trigonometric circle
   with thick line.

   .. note::

      Multiple range highlighting styles are not supported yet.

Example
=======

.. image:: _asy/trigcircle.png
   :align: right
.. literalinclude:: _asy/trigcircle.asy

.. note::

   Conversion from pdf to png lost quality. Actual pdf looks much better.
