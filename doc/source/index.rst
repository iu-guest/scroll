.. libasy documentation master file, created by
   sphinx-quickstart on Wed Aug 16 19:59:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libasy2's documentation!
===================================

Project libasy2 is a library for Asymptote vector graphics language.
It was born when set of functions (collectively known as `libasy`),
written and used for creating illustrations for mathematics textbook,
became too unwieldy and unmanageble.

.. note::

   Sphinx does not provide support for Asymptote language, but has
   sophisticated tools for C++ language, and since these languages are
   rather similar, concerning function signatures, this documentation
   is written with C++ directives. Unfortunately, among other
   shortcomings, it causes misleading "C++ function" references to
   appear in indexes. Ignore them.

   Sorry for inconvenience; contribution of proper Asymptote domain
   for Sphinx is welcome.

.. toctree::
   :maxdepth: 2

   string
   boxed
   ratio
   trigcircle
   generics

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
