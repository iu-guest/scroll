access libasy2;
unravel libasy2.trigcircle;
size(7cm);

var t = new trigcircle;
t.add_points(1::6, 5::6, 1::2, 3::2);
t.add_range(1 :: 2, 3 :: 2);

draw(t);
