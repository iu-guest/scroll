import useful;
import geometry;
import graph;
size(10cm);

real h = 0.5;
line a = line((-2, 0), (2, 0));
draw((-1.3, 0) -- (1.3, 0));
label("$a$", (1, 0), NW);
defpoint("A", (0, h), 1.5N + W);
circle c = new circle;
c.C = A;
c.r = 1.25h;
draw(c);
defpoint("M", intersectionpoints(a, c)[0], 2SW);
defpoint("N", intersectionpoints(a, c)[1], 2SE);

circle c1 = new circle;
circle c2 = new circle;
c1.r = length(M - A);
c1.C = M;
draw(c1, dashed);
c2.r = length(M - A);
c2.C = N;
draw(c2, dashed);
defpoint("L", intersectionpoints(c1, c2)[0], 3E);

defpoint("P", intersectionpoints(line(L, A), c)[0], SW);
defpoint("Q", intersectionpoints(line(L, A), c)[1], NE);
draw(L -- Q, dashed + linewidth(1.3));

c2.r = c1.r = length(P - Q);
c1.C = P;
c2.C = Q;
draw(c1, linewidth(1.5));
draw(c2, linewidth(1.5));
defpoint("U", intersectionpoints(c1, c2)[0], W);
defpoint("V", intersectionpoints(c1, c2)[1], E);
draw(U -- V, linewidth(1.5) + dotted);
