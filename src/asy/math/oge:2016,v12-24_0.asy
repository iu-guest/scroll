import useful;
import geometry;
size(10cm);

defpoint("B", (0, 0), E);
defpoint("A", (0, 3), E);
defpoint("C", (-3*sqrt(3), 0), W);

triangle t = triangle(B, A, C);
draw(t);
point H = intersectionpoint(altitude(t.vertex(1)), line(A, C));
label("$H$", H, N);
draw(B -- H);
draw(orth(H, B, A, scale = 0.1));
draw(orth(B, C, A, scale = 0.1));
draw_angle(A, C, B, scale = 0.1);
