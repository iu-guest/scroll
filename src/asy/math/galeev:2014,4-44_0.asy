import useful;
import _range;
import _intervals;
size(10cm);

var I = toggling_intervals
  (plus = GrassHighlighter + IncreaseHighlighter,
   minus = DecreaseHighlighter,
   in(0),
   in(16/5, label = "$\scriptstyle{}\frac{16}{5}$")
   );
I.push(in(-2) -- in(2),
       GrassHighlighter(bottom = true,
                        slant = 1,
                        height = 0.01));
draw(I);
label("$\scriptstyle f'(y)$", (-1.1, 0), N);
label("$\scriptstyle f(y)$", (-1.1, 0), S);
label("$\scriptstyle y$", (1.03, 0), E);
