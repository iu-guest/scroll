import geometry;
size(10cm);

point pA = (-4, 0);
point pD = (4, 0);
real h = 4;
real bx = -2;
point pB = (bx, h);
point pC = (bx + 3, h);

draw(pA -- pB -- pC -- pD -- cycle);
label("$A$", pA, W);
label("$B$", pB, W);
label("$C$", pC, E);
label("$D$", pD, E);

real al = 2;

point pM = (pA * al + pB)/(1 + al);
point pN = (pD * al + pC)/(1 + al);
label("$M$", pM, W);
label("$N$", pN, E);
draw(pM -- pN);

point pO = intersectionpoint(line(pA, pC), line(pB, pD));
draw(pA -- pC);
draw(pB -- pD);
label("$O$", pO, N);

point pK = intersectionpoint(line(pM, pN), line(pA, pC));
point pL = intersectionpoint(line(pM, pN), line(pD, pB));

label("$K$", pK, S);
label("$L$", pL, S);


point pH2 = intersectionpoint(line(pM, pN), altitude(triangle(pA, pM, pN).vertex(1)));
draw(pM -- pH2, dotted);
draw(pA -- pH2, dashed);
label("$h_2$", pA -- pH2, W);

point pH1 = intersectionpoint(line(pB, pC), altitude(triangle(pN, pB, pC).vertex(1)));
draw(pC -- pH1, dotted);
draw(pN -- pH1, dashed);
label("$h_1$", pN -- pH1, E);
label("$x$", pM -- pK, N);
label("$3x$", pK -- pL, S);
label("$x$", pL -- pN, N);
label("$8$", pA -- pD, S);
label("$3$", pB -- pC, N);

markangle(line(pK, pM), line(pA, pK), radius = 0.5 * markangleradius(currentpen));
markangle(line(pC, pB), line(pA, pK), radius = 0.5 * markangleradius(currentpen));

markangle(2, line(pL, pD), line(pN, pL), radius = 0.5 * markangleradius(currentpen));
markangle(2, line(pB, pD), line(pC, pB), radius = 0.5 * markangleradius(currentpen));




