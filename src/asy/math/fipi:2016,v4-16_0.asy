import useful;

size(10cm);

real R = 5;
real r = R/2;
path big_circle = circle((0, 0), R);
path small_circle = circle((-r, 0), r); draw(big_circle);
draw(small_circle);

pair m = (R / sqrt(2), R / sqrt(2));
real phi = pi * 1.319;
// It is ugly. Do it in principal way.
pair n = (R * cos(phi), R * sin(phi));
label("$M$", m, E);
label("$N$", n, SW);
draw(m -- n);
pair c = point(small_circle, intersections(small_circle, m, n)[0]);
label("$C$", c, SE);
pair k = (-R, 0);
pair o = (0, 0);
pair q = (R, 0);
draw(k -- q);
label("$K$", k, W);
label("$O$", o, NE);
label("$Q$", q, E);

pair a = point(small_circle, intersections(small_circle, m, k)[0]);
pair b = point(small_circle, intersections(small_circle, k, n)[1]);
label("$A$", a, N);
label("$B$", b, SW);
draw(k -- m -- n -- cycle);
draw(b -- o -- k -- cycle);
draw(b -- a -- k -- cycle);
draw(k -- m -- q -- cycle);
draw(orth(b, k, o));
draw(orth(m, k, q, scale = 0.1));
draw_angle(m, n, q, repeat = 2);
draw_angle(k, n, q, repeat = 2);
draw_angle(m, k, n);
draw_angle(o, k, b);
draw_angle(a, k, b);

pair l = point(a -- b, intersections(a -- b, k, c)[0]);
draw(k -- c);
label("$L$", l, 1.5N + 0.5W);
draw(a -- o, dashed);
