import geometry;
import useful;
size(10cm);

real al = 1.5;

point A = (-1, 0);
point C = (1, 0);
point B = (0, sqrt(4 * al *al - 1));

draw(A -- B -- C -- cycle);
label("$A$", A, W);
label("$C$", C, S);
label("$B$", B, W);
circle ex = excircle(B, C, A);
point O = ex.C;
draw(excircle(B, C, A));
label("$O$", O, E);
point F = intersectionpoint(line(A, B), altitude(triangle(O, A, B).vertex(1)));
draw(B -- F -- O);
point G = intersectionpoint(line(A, C), altitude(triangle(O, A, C).vertex(1)));
draw(C -- G -- O);
label("$F$", F, W);
label("$G$", G, S);
perpendicularmark(line(F, A), line(O, F));
perpendicularmark(line(G, O), line(A, G)) ;
pair H = intersectionpoint(B -- C, plain.circle(ex.C, ex.r));
draw(O -- H);
perpendicularmark(line(H, C), line(H, O));
label("$H$", H, NE + N);
draw(A -- O, dotted);
draw(B -- O, dotted);
draw(C -- O, dotted);
point K = intersectionpoint(line(A, O), line(B, C));
label("$K$", K, SW + S);

triangle abc = triangle(A, B, C);
circle incirc = incircle(abc);
draw(incirc);
point Q = incirc.C;
draw(A -- Q);
label("$Q$", Q, SE);
point V = intersectionpoint(A -- B, plain.circle(Q, incirc.r));
draw(Q -- V);
label("$V$", V, W);
draw(orth(V, A, Q));
point T = intersectionpoint(line(B, Q), line(A, C));
label("$T$", T, S);
draw(B -- T);
draw(orth(T, B, C));


