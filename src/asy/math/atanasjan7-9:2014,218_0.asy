import geometry;
import useful;
size(10cm);

pen thick = linewidth(1.3);
draw((-1.5, 0) -- (1.5, 0), thick);
draw((-1, -1) -- (1, 1));
draw((0, -1) -- (2, 1));
label("$a$", (-1.5, 0), NE);
label("$b$", (1, 1), 2W);
label("$c$", (2, 1), SE);
