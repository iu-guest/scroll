import useful;
import graph;
import geometry;
size(10cm);


defpoint("B", (0, 2), N);
defpoint("A", (-2 sqrt(3), 0), W);
defpoint("C", (2 sqrt(3), 0), E);

triangle T = triangle(A, B, C);
draw(T);

circle circleInAngle(point C, point A, point B, real r)
{
  pair U = C + unit(A - C) + unit(B - C);
  real d = distance(U, line(C, A));
  pair O = C + (U - C) * r / d;
  circle c;
  c.C = O;
  c.r = r;
  return c;
}

real r = 0.633;
circle C1 = circleInAngle(C, A, B, r);
circle C2 = circleInAngle(B, A, C, r);
defpoint('F', intersectionpoint(C1, B--C), N);
defpoint('G', intersectionpoint(C2, B -- C), NE);
pair O1 = C1.C;
pair O2 = C2.C;
label("$O_1$", O1, S);
label("$O_2$", O2, S);
draw(O1 -- O2);
draw(B -- O2);
draw(C -- O1);
draw(G -- O2);
draw(F -- O1);
draw(orth(F, C, O1));
draw(orth(G, O2, B));
draw(C1);
draw(C2);
