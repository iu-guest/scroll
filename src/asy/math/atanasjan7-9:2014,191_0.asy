import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", triangle(radians(40), radians(80)));
defpoint("K", intersectionpoint(bisector(ABC.vertex(2)), sg_AC), E);
draw(B -- K);
markangle(C, B, K, n = 1);
markangle(K, B, A, n = 1);

defpoint("M", intersectionpoint(sg_BC, line(K, K + B - A)), S);
draw(segment(K, M), marker = StickIntervalMarker(i = 1));
draw(segment(B, M), marker = StickIntervalMarker(i = 1));

markangle(B, K, M, n = 1);
