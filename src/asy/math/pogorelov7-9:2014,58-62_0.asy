import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), SW);
defpoint("B", (0.5, 2), NW);
defpoint("C", (2.2, 2), NE);
defpoint("D", (2.7, 0), SE);
draw(A -- B -- C -- D -- cycle);
draw(segment(A, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(C, D), marker = StickIntervalMarker(i = 1, n = 2));
defaltitude("H", triangle(A, B, D).vertex(2));
defaltitude("K", triangle(A, C, D).vertex(2));

label("\Large 2.7", A -- D);
label("\Large 1", B -- A, 3W);
markangle("$60\degree$", D, A, B);
markangle("$30\degree$", A, B, H, radius = 2.5markangleradius);
