import graph;
size(10cm);

xaxis("$x$", LeftTicks, EndArrow);
yaxis("$y$", LeftTicks, EndArrow);
pen bold = linewidth(2);
draw(graph(new real (real x) { return x; }, -5, 5), bold);
draw(graph(new real (real x) { return 1 - x;}, 0, 1), bold);
draw(graph(new real (real x) { return 1 - x;}, -3, 3), dashed);

draw(graph(new real (real x) { return -x*x + x + 1;}, 0, 1), bold);
draw(graph(new real (real x) { return -x*x + x + 1;}, -2, 3), dashed);
draw(graph(new pair (real y) { return (-y*y + y +1, y); }, 0, 1), bold);
draw(graph(new pair (real y) { return (-y*y + y +1, y); }, -2, 3), dashed);

draw((-5, -1) -- (5, -1), dotted);
draw((-5, 1) -- (5, 1), dotted);
draw((1, -5) -- (1, 5), dotted);
draw((-1, -5) -- (-1, 5), dotted);
