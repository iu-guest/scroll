import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (-2, 0), W);
defpoint("B", (2, 0), E);
circle c1 = new circle;
c1.C = A;
c1.r = 3;
draw(c1);
circle c2 = new circle;
c2.C = B;
c2.r = 2;
draw(c2);

point points[] = intersectionpoints(c1, c2);
point F = points[0];
point G = points[1];
label("$F$", F, 2S + 0.5E);
label("$G$", G, 2N + 0.5E);
draw(A -- B);
draw(A -- G -- B -- F -- cycle);
label("$3$", A -- F);
label("$3$", G -- A);
label("$2$", F -- B);
label("$2$", B -- G);
