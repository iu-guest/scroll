import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", (0, 0), (1, 1), (-1, 0));
circle c = new circle;
c.C = (A + B)/2;
c.r = length((A - B)/2);
draw(c, dashed + linewidth(2));
c.C = (B + C)/2;
c.r = length((C - B)/2);
draw(c, dotted + linewidth(3));
c.C = (A + C)/2;
c.r = length((A - C)/2);
draw(c);
