import useful;
import geometry;
import graph;
size(10cm);

real h = 25;
real x = 6;
defpoint("A", (0, h), W);
defpoint("B", (12, h), E);
defpoint("C", (x, 0), E);
defpoint("D", (x - 48, 0), W);
draw(A -- B);
draw(C -- D);
label("12", A -- B, N);
label("48", C -- D, S);

draw(A -- C);
draw(B -- D);
defpoint("M", intersectionpoint(line(A, C), line(B, D)), SE);
draw_angle(B, A, M, scale = 0.3);
draw_angle(D, C, M);
draw_angle(M, B, A, repeat = 2, scale = 0.3);
draw_angle(M, D, C, repeat = 2, scale = 0.08);
