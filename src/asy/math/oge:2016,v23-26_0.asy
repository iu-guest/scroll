import useful;
import geometry;
import graph;
size(10cm);

point O1 = (0, 0);
circle c1 = new circle;
c1.C = O1; c1.r = 12;
circle c2 = new circle;
c2.r = 20;
point O2 = (c1.r + c2.r, 0);
c2.C = O2;
draw(c1);
draw(c2);
real phi = asin((c1.r - c2.r)/(c2.r + c1.r));
vector radius = (vector)(tan(phi), 1);
defpoint("A", intersectionpoints(c1, tangent(c1, radius))[0], N);
defpoint("C", intersectionpoints(c2, tangent(c1, radius))[0], N);
vector radius = (vector)(tan(phi), -1);
defpoint("B", intersectionpoints(c1, tangent(c1, radius))[0], S);
defpoint("D", intersectionpoints(c2, tangent(c1, radius))[0], S);
draw(O1 -- A -- C -- O2 -- D -- B -- O1 -- O2 -- cycle );
markrightangle(O1, A, C);
markrightangle(O2, C, A);
markrightangle(O2, D, B);
markrightangle(O1, B, D);



label("$O_1$", O1, SE);
label("$O_2$", O2, E);

draw(C -- D, dashed);
draw(A -- B, dashed);
defaltitude("H", triangle(O1, O2, C).vertex(1));
draw(O1 -- intersectionpoint(line(A, B), line(O1, O2)));
defpoint("U", intersectionpoint(line(A, C), line(B,D)), W);
draw(U -- A);
draw(U -- O1);
draw(U -- B);
defpoint("K", intersectionpoint(line(U, O2), line(A, B)), SW);
defpoint("P", intersectionpoint(line(U, O2), line(C, D)), SW);
markrightangle(U, K, A);
markrightangle(U, P, C);
