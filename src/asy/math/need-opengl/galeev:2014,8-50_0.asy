import _stereo;
import three;
import useful;
size3(10cm);

var c = parallelepiped(2, 1.5, 1);
unravel c;
label(c);
Eye.add_object(c);
triple camera = (-5, 6, 2.25);
var M = (8A + 11A1)/19;
label("$M$", M, Z + X);
var P = (B1 + 2B)/3;
label("$P$", P, 2Y + 2Z);
var T = B - M + D1;
label("$T$", T, Z - 4Y);
Eye.add_path(B -- M -- D1 -- T -- cycle);
Eye.add_path(M -- P -- T);
Eye.add_path(D1 -- A);
Eye.add_path(D1 -- B);
draw(P -- D1, dashed);
draw(D1 -- C, dashed);

currentprojection = perspective(camera);
Eye.draw(camera);
