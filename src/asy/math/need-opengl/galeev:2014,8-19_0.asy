import three;
import useful;
size3(10cm);


// Outcircle radius of base
real R = 2 * sqrt(7/3);
// incircle of base
real r = R/2;
triple A = (R * sin(0), R * cos(0), 0);
triple B = (R * sin(2pi/3), R * cos(2pi/3), 0);
triple C = (R * sin(4pi/3), R * cos(4pi/3), 0);
real h = 6; // height of pyramid
triple S = (0, 0, h);
triple O = (0, 0, 0);
draw(A -- B -- C -- cycle, dashed);
label("$A$", A, E);
label("$\scriptstyle B$", B, NE);
label("$C$", C, W);
label("$S$", S, N);
label("$O$", O, W);
draw(S -- O, dashed);

for (triple p: new triple[]{A, B, C}) {
  draw(p -- S, dashed);
}
draw(S -- A -- C -- cycle);
draw(S -- A -- B -- cycle);

currentprojection = perspective(2.5, -2, 0);

triple T = (A + B)/2;
draw(O -- T, dashed);
label("$T$", T, E);
draw(S -- T);

triple R = (5T + S)/6;
label("$R$", R, NE);
draw(O -- R, dashed);
draw(R -- B);
