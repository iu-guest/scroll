import useful;
import graph;
import geometry;
size(10cm);


defpoint("B", (0, 2), N);
defpoint("A", (-2 sqrt(3), 0), W);
defpoint('C', (2 sqrt(3), 0), E);
defpoint('H', (A+C)/2, S);


draw(A -- B -- C -- cycle);
draw(B -- H);

triangle T1 = triangle(A, B, H);
triangle T2 = triangle(B, C, H);
draw(incircle(T1));
draw(incircle(T2));


defpoint('F', intersectionpoint(incircle(T1), incircle(T2)), E);
