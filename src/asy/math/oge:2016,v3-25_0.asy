import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

quadrilateral tr = trapezium(pi/4, pi/3, 0.3);
draw(tr);
unravel tr;
defpoint("M", (A + B)/2, NW);
defpoint("N", (C + D)/2, NE);
draw(M -- N);
defpoint("F", (2M + 5N)/7, 2S + W);
draw(A -- F -- B -- C -- F -- D --cycle);
defaltitude("P", triangle(B, F, C).vertex(2));
defaltitude("Q", triangle(A, F, D).vertex(2));
