import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", triangle(radians(67), radians(83)));
circle c = circle(A, B, C);
draw(c);
path a = angle(B, C , A);
draw(a);
label("$67\degree$", a);
path a = angle(C, A , B);
draw_angle(C, A, B, repeat = 2);
draw(a);
label("$83\degree$", a);
