import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), S);
defpoint("D", (3, 0), S);
point off = (3, 2);
point B1 = A + off;
point C1 = D + off;

draw(A -- D);
line ab = bisector(line(A, B1), line(A, D));
line db = bisector(line(D, C1), line(D, A), sharp = false);
defpoint("E", intersectionpoint(ab, db), N);
draw(A -- D -- E -- cycle);
defpoint("B", intersectionpoint(line(A, B1), line(E, E + plain.E)), N);
defpoint("C", intersectionpoint(line(D, C1), line(E, E + plain.E)), N);
draw(A -- B -- C -- D -- cycle);
markangle(D, A, E, radius = 1.75 markangleradius);
markangle(E, A, B, radius = 2markangleradius);
markangle(B, E, A, radius = 2markangleradius);

markangle(E, D, A, n = 2, radius = 0.4 markangleradius);
markangle(C, D, E, n = 2, radius = 0.6 markangleradius);
markangle(D, E, C, n = 2, radius = 0.6 markangleradius);
