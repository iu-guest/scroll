access libasy2;
unravel libasy2.trigcircle;
size(7cm);

var t = new trigcircle;
t.add_points(1::6, 5::6, 1::2, 3::2);
t.add_range(1 :: 2, 3 :: 2);
t.add_point(1 :: 3, false);
t.add_point(5 :: 4, false);

draw(t);
