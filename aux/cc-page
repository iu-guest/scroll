#!/bin/sh
# Copyright (C) 2017 Dmitry Bogatov <KAction@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# tup build system is very keen on principle 'one output file from one
# rule'.  While it is beautiful in general, unfortunately TeX works in
# absolutely incompatible way, creating too many auxiliary files, in
# particular for files, input'ed by several rules.

# This script is designed to hide ugliness of TeX from tup, making it
# look almost like sane compiler. It accepts single argument --
# basename of TeX file under math/, compiles everything as required in
# temporary directory and places resulting pdf under out/.

set -eu
SOURCE=$1; shift 1
NAME=$(basename "$SOURCE" .tex)

TEMPDIR=$(mktemp -d -p /dev/shm)
trap cleanup EXIT
cleanup () {
	rm -rf "$TEMPDIR"
}

install -t "$TEMPDIR" LaTeX/*.tex LaTeX/*.sty
ln -s $PWD/out/pictures "$TEMPDIR/pictures"
ln -s $PWD/out/fragment "$TEMPDIR/fragment"



# Symlink $2- argument (other style packages)
for f ; do
	ln -s "${PWD}/${f}" "$TEMPDIR/"
done

IFS=:-,
eval set -- $NAME
case "$1" in
	chemistrymsu)
		echo "\\declaretask[task=$3,paragraph=$2]{$1}" > "$TEMPDIR/$NAME.tex"
		;;
	galeev|msu)
		echo "$@"
		echo "\\declaretask[task=$3.$4,year=$2]{$1}" > "$TEMPDIR/$NAME.tex"
		;;
	ml)
	    echo "$@"
	    echo "\\declaretask[variant=$2,paragraph=$3,task=$4]{$1}" > "$TEMPDIR/$NAME.tex"
	    ;;

esac
cat "$SOURCE" >> "$TEMPDIR/$NAME.tex"

cat <<EOF > "$TEMPDIR/dummy.tex"
\def\thesource{$NAME}
\input main.tex
EOF

cd "$TEMPDIR"
xelatex dummy.tex
cd -
cp -- "$TEMPDIR/dummy.pdf" "out/$NAME.pdf"
