// Copyright (C) 2017 Dmitry Bogatov
//
// This file is part of libasy2.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

private string char_lower(string c)
{
	assert(length(c) == 1);

	if (c == 'A') return 'a';
	if (c == 'B') return 'b';
	if (c == 'C') return 'c';
	if (c == 'D') return 'd';
	if (c == 'E') return 'e';
	if (c == 'F') return 'f';
	if (c == 'G') return 'g';
	if (c == 'H') return 'h';
	if (c == 'I') return 'i';
	if (c == 'J') return 'j';
	if (c == 'K') return 'k';
	if (c == 'L') return 'l';
	if (c == 'M') return 'm';
	if (c == 'N') return 'n';
	if (c == 'O') return 'o';
	if (c == 'P') return 'p';
	if (c == 'Q') return 'q';
	if (c == 'R') return 'r';
	if (c == 'S') return 's';
	if (c == 'T') return 't';
	if (c == 'U') return 'u';
	if (c == 'V') return 'v';
	if (c == 'W') return 'w';
	if (c == 'X') return 'x';
	if (c == 'Y') return 'y';
	if (c == 'Z') return 'z';

	return c;
}

private string char_upper(string c)
{
	assert(length(c) == 1);

	if (c == 'a') return 'A';
	if (c == 'b') return 'B';
	if (c == 'c') return 'C';
	if (c == 'd') return 'D';
	if (c == 'e') return 'E';
	if (c == 'f') return 'F';
	if (c == 'g') return 'G';
	if (c == 'h') return 'H';
	if (c == 'i') return 'I';
	if (c == 'j') return 'J';
	if (c == 'k') return 'K';
	if (c == 'l') return 'L';
	if (c == 'm') return 'M';
	if (c == 'n') return 'N';
	if (c == 'o') return 'O';
	if (c == 'p') return 'P';
	if (c == 'q') return 'Q';
	if (c == 'r') return 'R';
	if (c == 's') return 'S';
	if (c == 't') return 'T';
	if (c == 'u') return 'U';
	if (c == 'v') return 'V';
	if (c == 'w') return 'W';
	if (c == 'x') return 'X';
	if (c == 'y') return 'Y';
	if (c == 'z') return 'Z';

	return c;
}

private bool char_is_lower(string c)
{
	assert(length(c) == 1);

	if (c == 'a') return true;
	if (c == 'b') return true;
	if (c == 'c') return true;
	if (c == 'd') return true;
	if (c == 'e') return true;
	if (c == 'f') return true;
	if (c == 'g') return true;
	if (c == 'h') return true;
	if (c == 'i') return true;
	if (c == 'j') return true;
	if (c == 'k') return true;
	if (c == 'l') return true;
	if (c == 'm') return true;
	if (c == 'n') return true;
	if (c == 'o') return true;
	if (c == 'p') return true;
	if (c == 'q') return true;
	if (c == 'r') return true;
	if (c == 's') return true;
	if (c == 't') return true;
	if (c == 'u') return true;
	if (c == 'v') return true;
	if (c == 'w') return true;
	if (c == 'x') return true;
	if (c == 'y') return true;
	if (c == 'z') return true;

	return false;
}

private bool char_is_upper(string c)
{
	assert(length(c) == 1);


	if (c == 'A') return true;
	if (c == 'B') return true;
	if (c == 'C') return true;
	if (c == 'D') return true;
	if (c == 'E') return true;
	if (c == 'F') return true;
	if (c == 'G') return true;
	if (c == 'H') return true;
	if (c == 'I') return true;
	if (c == 'J') return true;
	if (c == 'K') return true;
	if (c == 'L') return true;
	if (c == 'M') return true;
	if (c == 'N') return true;
	if (c == 'O') return true;
	if (c == 'P') return true;
	if (c == 'Q') return true;
	if (c == 'R') return true;
	if (c == 'S') return true;
	if (c == 'T') return true;
	if (c == 'U') return true;
	if (c == 'V') return true;
	if (c == 'W') return true;
	if (c == 'X') return true;
	if (c == 'Y') return true;
	if (c == 'Z') return true;

	return false;
}

private bool char_is_digit(string c)
{
	assert(length(c) == 1);

	if (c == '0') return true;
	if (c == '1') return true;
	if (c == '2') return true;
	if (c == '3') return true;
	if (c == '4') return true;
	if (c == '5') return true;
	if (c == '6') return true;
	if (c == '7') return true;
	if (c == '8') return true;
	if (c == '9') return true;

	return false;
}

private bool char_is_whitespace(string c)
{
	assert(length(c) == 1);

	if (c == '\t') return true;
	if (c == '\v') return true;
	if (c == '\n') return true;
	if (c == ' ') return true;

	return false;
}

private bool char_is_alpha(string c)
{
	assert(length(c) == 1);
	return char_is_lower(c) || char_is_upper(c);
}

private bool every_char(string s, bool test (string c))
{
	/* Special case, as in Python string library. */
	if (length(s) == 0)
		return false;

	for (int ix = 0; ix != length(s); ++ix) {
		string c = substr(s, ix, 1);
		if (!test(c))
			return false;
	}
	return true;
}

public string operator *(string s, int n)
{
	string res = "";
	for (int i = 0; i != n; ++i)
		res += s;
	return res;
}

public string concat_map(string s, string fn (int index, string c))
{
	var len = length(s);
	var res = "";

	for (int ix = 0; ix != len; ++ix) {
		string c = substr(s, ix, 1);
		res += fn(ix, c);
	}
	return res;
}

public string concat_map(string s, string fn (string c))
{
	return concat_map(s, new string (int _, string c) { return fn(c); });
}

private string capitalize_trans(int index, string c)
{
	if (index == 0)
		return char_upper(c);
	else
		return char_lower(c);
}

public string capitalize(string s)
{
	return concat_map(s, capitalize_trans);
}

public string center(string s, int width, string fill = ' ')
{
	assert(length(fill) == 1,
	       "fill character must be exactly one character long");

	int padding = width - length(s);
	if (padding <= 0) {
		return s;
	} else {
		int before = padding # 2;
		int after  = padding - before;
		return fill * before + s + fill * after;
	}

}

public string slice(string s, int begin = 0, int end = length(s))
{
	if (end < 0)
		end += length(s);
	if (begin < 0)
		begin += length(s);
	if (begin < 0 || end < 0 || begin >= end)
		return "";
	return substr(s, begin, end - begin);
}

public int count(string s, string sub, int begin = 0, int end = length(s))
{
	s = slice(s, 0, end);
	int result = 0;

	int ix = begin;
	while ((ix = find(s, sub, ix)) != -1) {
		ix += length(sub);
		result += 1;
	}
	return result;
}

public bool endswith(string s, string suffixes[],
		     int start = 0, int end = length(s))
{
	if (start < 0)
		start += length(s);
	if (end < 0)
		end += length(s);
	if (start < 0 || end < 0 || start >= end)
		return false;

	for (var suffix: suffixes) {
		int suffix_len = length(suffix);
		if (suffix_len > end - start)
			continue;
		if (substr(s, end - suffix_len, suffix_len) == suffix)
			return true;
	}

	return false;
}

public bool endswith(string s, int start = 0, int end = length(s),
		     string suffix ... string[] suffixes)
{
	suffixes.push(suffix);
	return endswith(s, suffixes, start, end);
}

public bool isalpha(string s)
{
	return every_char(s, char_is_alpha);
}

public bool isdigit(string s)
{
	return every_char(s, char_is_digit);
}

private typedef bool charp(string c);
private charp negate(charp test)
{
	return new bool (string c) { return !test(c); };
}

public bool islower(string s)
{
	return every_char(s, negate(char_is_upper));
}

public bool isupper(string s)
{
	return every_char(s, negate(char_is_lower));
}

public string join(string sep, string[] elements)
{
	if (elements.length == 0)
		return "";

	string result = "";
	for (int ix = 0; ix != elements.length - 1; ++ix)
		result += elements[ix] + sep;
	result += elements[elements.length - 1];
	return result;
}

public string join(string sep... string[] elements)
{
	return join(sep, elements);
}

public string lower(string s)
{
	return concat_map(s, char_lower);
}

private struct subst {
	public string with;
	public string what;
};

private subst make_subst(string what, string with)
{
	var res = new subst;
	res.with = with;
	res.what = what;

	return res;
}

public subst operator cast(string s)
{
	return make_subst(what = '', with = s);
}

public subst operator cast(int n)
{
	return make_subst(what = '', with = (string) n);
}

public subst operator % (string what, string with)
{
	return make_subst(what, with);
}

private void sprintf_assign_indexes(subst[] substitutions)
{
	int index = 0;
	for (var st: substitutions)
		if (st.what == '') {
			st.what = string(index);
			index += 1;
		}
}

public string sprintf(string template, subst[] substitutions)
{
	string[][] replacements;
	sprintf_assign_indexes(substitutions);
	for (var st: substitutions) {
		replacements.push(new string[] { "{" + st.what + "}", st.with });
	}
	return replace(template, replacements);
}

public void evalf(string template... subst[] substitutions)
{
	eval(sprintf(template, substitutions), true);
}

public string sprintf(string template... subst[] substitutions)
{
	return sprintf(template, substitutions);
}
