// Copyright (C) 2017 Dmitry Bogatov
//
// This file is part of libasy2.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

access "libasy2/ratio.asy" as ratio;
unravel ratio;
access "libasy2/string.asy" as str;
unravel str;

private ratio min(ratio a, ratio b) { return (a > b) ? b : a; }
private ratio max(ratio a, ratio b) { return (a > b) ? a : b; }

private struct range {
	restricted ratio left;
	restricted ratio right;

	void operator init(ratio left, ratio right) {
		this.left  = min(left, right);
		this.right = max(left, right);
	}
};

private Label make_label(ratio r)
{
	if (r == 0)
		return "$0$";
	if (r == 1)
		return '$\\pi$';
	if (r == -1)
		return '$-\\pi$';

	if (r.denominator == 1)
		return sprintf('${1}\\pi$', r.numerator);

	var sign = (r > 0) ? "" : "-";

	if (r.numerator == 1)
		return sprintf('${0}\\frac{\\pi}{ {1} }$', sign, r.denominator);
	return sprintf('${0}\\frac{ {1} }{ {2} }\\pi$',
		       sign,
		       abs(r.numerator),
		       abs(r.denominator));
}

private pair getpos(ratio r)
{
	var arg = pi * r.numerator / r.denominator;
	return (cos(arg), sin(arg));
}

private void label(ratio r)
{
	pair pos = getpos(r);
	label(make_label(r), pos, pos);
}

private void markpoint(ratio r, bool inc = true)
{
	pair pos = getpos(r);
	path p = shift(pos) * scale(0.018) * unitcircle;
	fill(p, inc ? black : white);
	draw(p);
}

struct trigcircle {
	restricted ratio[] includes;
	restricted ratio[] excludes;
	restricted range[] ranges;
	public void add_point(ratio p, bool inc = true) {
		var list = inc ? this.includes : this.excludes;
		list.push(p);
	}
	public void add_points(...ratio[] ps) {
		for (var p: ps)
			this.add_point(p);
	}
	public void add_range(ratio left, ratio right) {
		this.ranges.push(range(left, right));
	}
};

void draw(trigcircle t)
{
	draw(unitcircle);
	draw((-1, 0) -- (1, 0));
	draw((0, -1) -- (0, 1));
	label("$\\cos$", (0, 0) -- (1, 0));
	label("$\\sin$", (0, 0) -- (0, 1));
	for (var rng: t.ranges) {
		var posl = getpos(rng.left);
		var posr = getpos(rng.right);
		draw(arc((0, 0), posl, posr), linewidth(1.25));
	}
	for (var r: t.includes) {
		label(r);
		markpoint(r, true);
	}
	for (var r: t.excludes) {
		label(r);
		markpoint(r, false);
	}
}
