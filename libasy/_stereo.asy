import three;
import geometry;

struct prism {
	restricted triple base[];
	restricted triple side;
	void operator init(triple side...triple[] base) {
		this.side = side;
		this.base = base;
	};
};

private path3 path3(triple offset, triple[] points) {
	path3 p;
	for (triple A: points) {
		p = p -- (A + offset);
	}
	return p -- cycle;
}

struct prism4 {
	restricted triple A;
	restricted triple A1;
	restricted triple B;
	restricted triple B1;
	restricted triple C;
	restricted triple C1;
	restricted triple D;
	restricted triple D1;
	void operator init(triple side, triple A, triple B, triple C, triple D) {
		this.A = A; this.A1 = A + side;
		this.B = B; this.B1 = B + side;
		this.C = C; this.C1 = C + side;
		this.D = D; this.D1 = D + side;
	}
}
prism operator cast(prism4 p) { return prism(p.A1 - p.A, p.A, p.B, p.C, p.D); }

struct pyramid {
	restricted triple base[];
	restricted triple vertex;
	void operator init(triple vertex ...triple base[]) {
		this.vertex = vertex;
		this.base = base;
	};
};
struct pyramid3 {
	restricted triple A;
	restricted triple B;
	restricted triple C;
	restricted triple S;
	void operator init(triple S, triple A, triple B, triple C) {
		this.S = S;
		this.A = A;
		this.B = B;
		this.C = C;
	}
};

private triple lift(point A, real z) { return (A.x, A.y, z); }
private point excenter(triangle T) { return excenter(T.A, T.B, T.C); }

pyramid3 pyramid3(triangle T, real h, point S1 = excenter(T)) {
	triple A = lift(T.A, -h/2);
	triple B = lift(T.B, -h/2);
	triple C = lift(T.C, -h/2);
	triple S = lift(S1, h/2);
	return pyramid3(S, A, B, C);
}

pyramid operator cast(pyramid3 p) { 
	unravel p;
	return pyramid(S, A, B, C);
}

private string letters = "ABCDEFGHIJ";
void label(prism p) {
	for (int i = 0; i != p.base.length; ++i) {
		 string c = substr(letters, i, 1);
		 string l1 = "$" + c + "$";
		 string l2 = "$" + c + "_1$";
		 label(l1, p.base[i], -Z);
		 label(l2, p.base[i] + p.side, Z);
	}
};
void label(pyramid p, Label vertex = "$D$") {
	for (int i = 0; i != p.base.length; ++i) {
		 string c = substr(letters, i, 1);
		 string l = "$" + c + "$";
		 label(l, p.base[i], -Z);
	}
	label(vertex, p.vertex, Z);
}

prism4 parallelepiped(real l, real w, real h) {
	triple A = (l/2, w/2, -h/2);
	triple B = (-l/2, w/2, -h/2);
	triple C = (-l/2, -w/2, -h/2);
	triple D = (l/2, -w/2, -h/2);
	return prism4((0, 0, h), A, B, C, D);
}

prism4 cube(real side=1.0) {
	return parallelepiped(side, side, side);
}

struct observer {
	static private triple plain[] = new triple[] {O, O, O, O};
	private surface obstacles[];
	private path3 paths[];
	
	void operator init() { 
		this.obstacles = new surface[];
		this.paths = new path3[];
	};

	void add_path(path3 p) { this.paths.push(p); }
	void add_obstacle(surface s) { this.obstacles.push(s); }
	void add_obstacle(path3 p) { this.add_obstacle(surface(p, this.plain)); }
	void add_obstacle(triple p1, triple p2 ...triple[] rest) {
		path3 external = p1 -- p2;
		for (triple p: rest)
			external = external -- p;
		external = external -- cycle;
		this.add_obstacle(external);
	}
	void add_object(prism p) {
		for (int i = 0; i != p.base.length; ++i) {
			 int j = (i + 1) % p.base.length;
			 triple A = p.base[i];
			 triple B = p.base[j];
			 triple A1 = A + p.side;
			 triple B1 = B + p.side;
			 path3 p = A -- B -- B1 -- A1 -- cycle;
			 this.add_obstacle(p);
			 this.add_path(p);
		};
		this.add_obstacle(path3(O, p.base));
		this.add_obstacle(path3(p.side, p.base));
	};
	void add_object(pyramid p) {
		for (int i = 0; i != p.base.length; ++i) {
			 int j = (i + 1) % p.base.length;
			 triple A = p.base[i];
			 triple B = p.base[j];
			 path3 pth = A -- (A+B)/2 -- B -- p.vertex -- cycle;
			 this.add_obstacle(pth);
			 this.add_path(pth);
		}
	}
	private bool visible(triple camera, triple X) {
		static real eps = 1e-2;
		path3 segment = camera -- X;
		for (surface s: this.obstacles) {
			triple common[] = intersectionpoints(segment, s);
			if (common.length > 1)
				return false;
			if (common.length == 0)
				continue;
			if (length(common[0] - X) > eps)
				return false;
		}
		return true;
	}
	private void draw_path(path3 p, triple camera, pen visible, pen invisible) {
		int N = 250;
		bool previous = true;
		real begin = 0.0;
		for (int i = 1; i != N; ++i) {
			real at = length(p)/N * i;
			triple X = point(p, at);
			bool can_see = this.visible(camera, X);

			if (can_see != previous) {
				pen tool = previous ? visible : invisible;
				draw(subpath(p, begin, at), tool);
				previous = can_see;
				begin = at;
			}
		}
		bool can_see = this.visible(camera, point(p, length(p)));
		draw(subpath(p, begin, length(p)), can_see ? visible : invisible);
	}

	void draw(triple camera = (5, 4, 2), 
	          pen visible = currentpen, pen invisible = currentpen + dashed) {
		for (path3 p: this.paths) 
			this.draw_path(p, camera, visible, invisible);
	}
	
}

observer Eye = new observer;
